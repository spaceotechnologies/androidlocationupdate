# AndroidLocationUpdate

If you’ve read the part one of [Android location manager tutorial](https://www.spaceotechnologies.com/obtain-current-android-location-manager/), then we are assuming that you’ve successfully integrated maps in your Android app.

That’s cool, now we’ll obtain the current location of the device.

We've also written the part -2 of [obtaining current location with android location manager](https://www.spaceotechnologies.com/obtain-current-location-using-android-location-manager/), so you can check out the article as well.

In this part, we’ve added features of showing current location directly on the map.

If you face any issue implementing it, you can contact us for help. Also, if you want to implement this feature in your Android app and looking to [hire Android app developer](http://www.spaceotechnologies.com/hire-android-developer/), then you can contact Space-O Technologies for the same.